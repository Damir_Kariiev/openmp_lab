#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include <chrono>
#include <execution>

#include <omp.h>
#include <windows.h>


std::string ReadFile(const std::string& FilePath) {
    std::fstream in(FilePath);
    if (in.is_open())
    {
        std::ostringstream Stream;
        Stream << in.rdbuf();
        return Stream.str();
    }
    else
    {
        return {};
    }
}

int GetWordCount(char* Fragment, int FragmentCharAmount, char* WordIter, int StringCharAmount)
{
    int count = 0;
    int globalposition = 0;

    for (globalposition = 0; globalposition < FragmentCharAmount - StringCharAmount + 1; ++globalposition)
    {

        int localposition = 0;

        while (localposition != StringCharAmount &&
            toupper(*(WordIter + localposition)) == toupper(*(Fragment + globalposition + localposition)))
        {
            ++localposition;
        }

        count += (localposition == StringCharAmount);
    }

    return count;
}

int main()
{
    using namespace std::chrono;

    std::string Word;
    std::cin >> Word;

    std::string Text = ReadFile("input.txt");

    const int ThreadCounter = 8;
    const int Incrementer = Text.length() / ThreadCounter;
    int counter = 0;

    auto Start = steady_clock::now();

    #pragma omp parallel for reduction(+:counter)
    for (int i = 0; i < ThreadCounter; ++i)
    {
        const auto IterStart = Text.data() + i * Incrementer;

        counter += GetWordCount(IterStart, Incrementer, Word.data(), Word.length());
        std::cout << "Process " << i << " result: " << counter << '\n';
    }

    auto End = steady_clock::now();

    std::cout << duration_cast<microseconds>(End - Start).count() / 1000.0 << "ms\n";
    std::cout << counter << '\n';

    return 0;
}